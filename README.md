# **Welcome to YeAPF!**

    Copyright (C) 2004-2021 Esteban Daniel Dortta - dortta@yahoo.com - MIT License
    YeAPF 0.8.64-11 built on 2021-01-22 16:32 (-3 DST)
    Last file version: 2019-07-19 18:34:17 (-3 DST)

Welcome, you can read the [English readme file](readme-en.md)

Bem-vindo, você pode ler o [Arquivo readme em Português](readme-pt-br.md)

Bienvenido, puedes leer el [Archivo readme en Español](readme-es.md)

