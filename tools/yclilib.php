<?php
/*
tools/yclilib.php
YeAPF 0.8.64-11 built on 2021-01-22 16:32 (-3 DST)
Copyright (C) 2004-2021 Esteban Daniel Dortta - dortta@yahoo.com - MIT License
2020-04-18 19:20:43 (-3 DST)
 */

$PHPVer     = phpversion();
$PHPVersion = explode('.', $PHPVer);
$PHPVersion = $PHPVersion[0] * 10000 + $PHPVersion[1] * 100 + $PHPVersion[2];
unset($PHPVer);

define('_OWNER_CAN_READ', 0x0100);
define('_OWNER_CAN_WRITE', 0x0080);
define('_OWNER_CAN_EXEC', 0x0040);

define('_GROUP_CAN_READ', 0x0020);
define('_GROUP_CAN_WRITE', 0x0010);
define('_GROUP_CAN_EXEC', 0x0400);

define('_WORLD_CAN_READ', 0x0004);
define('_WORLD_CAN_WRITE', 0x0002);
define('_WORLD_CAN_EXEC', 0x0001);

function yclibErrorHandler($errno, $errstr, $errfile, $errline) {
	if (!(error_reporting() & $errno)) {
		// This error code is not included in error_reporting, so let it fall
		// through to the standard PHP error handler
		return false;
	}

	switch ($errno) {
	case E_USER_ERROR:
		echo "<b>My ERROR</b> [$errno] $errstr<br />\n";
		echo "  Fatal error on line $errline in file $errfile";
		echo ', PHP ' . PHP_VERSION . ' (' . PHP_OS . ")<br />\n";
		echo "Aborting...<br />\n";
		exit(1);
		break;
	case E_USER_WARNING:
		echo "<b>My WARNING</b> [$errno] $errstr<br />\n";
		break;
	case E_USER_NOTICE:
		echo "<b>My NOTICE</b> [$errno] $errstr<br />\n";
		break;
	default:
		echo "Unknown error type: [$errno] $errstr<br />\n";
		break;
	}

	/* Don't execute PHP internal error handler */
	return true;
}
$old_error_handler = set_error_handler('yclibErrorHandler');

function updateFiles($sourcePath, $pattern, $target = '.', $toForce = false) {
	global $appFolder;
	$specialFiles = ['yeapf.db.ini', 'search.path', 'e_index.html', 'e_main_menu.html', 'yapp.ini', 'config.ini'];
	echo "\r  --+ $sourcePath/$pattern\n";
	$files = glob("$sourcePath/$pattern");
	foreach ($files as $fileName) {
		$bName        = basename($fileName);
		$payAttention = (in_array($bName, $specialFiles)) ? ' *PA' : '';
		echo str_replace('//', '/', '    +-- ' . substr("$target/$bName", strlen($appFolder) + 1) . "  $payAttention                                  \r");
		if ((!$toForce) && (in_array($bName, $specialFiles))) {
			echo "X\n";
		} else {
			if (is_dir("$sourcePath/$bName")) {
				if (!(($bName === '.') || ($bName === '..'))) {
					echo "\n";
					updateFiles("$sourcePath/$bName", $pattern, "$target/$bName", $toForce);
				} else {
					echo "\n";
				}
			} else {
				if (!is_dir($target)) {
					echo "\nCreating '$target'\n";
					mkdir($target, 0777, true);
				}

				if (file_exists("$target/$bName")) {
					$t1 = filemtime("$sourcePath/$bName");
					$t2 = filemtime("$target/$bName");
					if (($t1 > $t2) || ($toForce)) {
						unlink("$target/$bName");
						copy("$sourcePath/$bName", "$target/$bName");
						echo "U\n";
					} else {
						echo "=\n";
					}
				} else {
					copy("$sourcePath/$bName", "$target/$bName");
				}
			}
		}
		echo '';
	}
}

function ver2value($aVer, &$updateSequence) {
	$ver = explode('.', $aVer);
	$aux = explode('-', $ver[2]);
	if (isset($aux[1])) {
		$updateSequence = $aux[1];
	} else {
		$updateSequence = 0;
	}
	$aVer[2] = $aux[0];
	$ret     = $ver[0] * 1000 + $ver[1] * 100 + (int) ($ver[2]) * 1;

	return $ret;
}

function value2ver($aValue, $updateSequence = 0) {
	$div = [1000, 100, 1];
	$ver = [];
	for ($i = 0; $i < 3; $i++) {
		$ver[$i] = floor($aValue / $div[$i]);
		$aValue -= $ver[$i] * $div[$i];
	}
	$updateSequence = (int) $updateSequence;

	return join('.', $ver) . '-' . $updateSequence;
}

$GLOBALS['__yeapfPath'] = '%_YEAPF_PATH_%';
/* this allows to use yclilib from YeAPF head source folder */
if ($GLOBALS['__yeapfPath'] === '%_' . 'YEAPF_PATH_%') {
	$GLOBALS['__yeapfPath'] = dirname(dirname(__FILE__));
}

if (file_exists('flags/timezone')) {
	$auxDefaultTimeZone = file_get_contents('flags/timezone');
} else {
	$auxDefaultTimeZone = @date_default_timezone_get();
}

if (!date_default_timezone_set($auxDefaultTimeZone)) {
	die("\nWas not possible to set timezone to '$auxDefaultTimeZone'\n");
}
unset($auxDefaultTimeZone);

function _LOAD_YEAPF_($libraryList = '') {
	global $__yeapfPath, $cfgAvoidIncludesLst, $flagDBStructureCanBeReviewed;

	/* avoid loading includes.lst in app folders */
	$cfgAvoidIncludesLst = 'yes';

	/* to be used when on development stage */
	if (substr($__yeapfPath, 0, 1) === '%') {
		$__yeapfPath = __DIR__ . '/../';
	}

	$flagDBStructureCanBeReviewed = false;

	echo "; Loading YeAPF from $__yeapfPath\n";
	(@include_once ("$__yeapfPath/includes/yeapf.functions.php")) or die("Impossible to load YeAPF functions\npath: $__yeapfPath\n");
	echo "; YeAPF ready\n";
	if ($libraryList > '') {
		$libraryList = explode(';', $libraryList);
		foreach ($libraryList as $libName) {
			(@include_once ("$__yeapfPath/includes/$libName")) or die("Impossible to load YeAPF functions\npath: $__yeapfPath\n");
		}
	}
}

define(__TRUE__, '__TRUE__');
define(__FALSE__, '__FALSE__');
function showError($errLevel, $errMessage) {
	syslog($errLevel, $errMessage);
	echo $errMessage;

	if ($errLevel >= LOG_CRIT) {
		die();
	}
}

class ArgumentsProcessor {
	protected $src;
	protected $dst;
	protected $options;

	public function __construct($lastParamIsTarget = true) {
		$this->parseArguments($lastParamIsTarget);
	}

	public function isParamValue($paramNdx) {
		global $argc, $argv;

		$ret = false;
		if ($paramNdx < $argc) {
			if (substr($argv[$paramNdx], 0, 1) !== '-') {
				$ret = true;
			}
		}

		return $ret;
	}

	public function parseArguments($lastParamIsTarget) {
		global $argv, $argc;

		$srcList       = [];
		$this->options = [];
		$this->src     = [];
		$this->dest    = [];

		$i = 1;
		while ($i < $argc) {
			$thisParam = 0;
			$thisParam = $argv[$i];
			if (substr($thisParam, 0, 1) === '-') {
				$thisType  = 2;
				$paramName = $thisParam;
				while (substr($paramName, 0, 1) === '-') {
					$paramName = substr($paramName, 1);
				}
				if ((substr($thisParam, 1, 1) === '-') && ($this->isParamValue($i + 1))) {
					$i++;
					$this->options[$paramName] = $argv[$i];
				} else {
					$this->options[$paramName] = __TRUE__;
				}
			} else {
				$thisType                 = 1;
				$srcList[count($srcList)] = $thisParam;
			}
			$i++;
		}

		if ((count($srcList) > 1) && ($lastParamIsTarget)) {
			$this->dest = $srcList[count($srcList) - 1];
			$srcList    = array_slice($srcList, 0, count($srcList) - 1);
		}

		$this->src = $srcList;
	}

	public function setArgValue($argName, $argValue) {
		$this->options[$argName] = $argValue;
	}

	public function argValue($argNameList, $defaultValue = false) {
		$ret     = $defaultValue;
		$argList = explode(';', $argNameList);
		foreach ($argList as $argName) {
			if (isset($this->options[$argName])) {
				$ret = $this->options[$argName];
			}
		}

		return $ret;
	}

	public function optionCount() {
		return count($this->options);
	}

	public function srcCount() {
		return count($this->src);
	}

	public function getSrc($srcNdx) {
		if (isset($this->src[$srcNdx])) {
			return $this->src[$srcNdx];
		}

		return null;
	}

	public function getOptionList() {
		$ret = [];
		foreach ($this->options as $k => $v) {
			if (mb_strpos($k, '=')) {
				$key          = explode('=', $k);
				$ret[$key[0]] = $key[1];
			} else {
				$ret[$k] = $v;
			}
		}

		return $ret;
	}
}

function expandArguments($tag, &$tagList) {
	global $nodes;

	$tagList = [];
	$p       = strpos($tag, ':');
	if ($p !== false) {
		$nList = substr($tag, 0, $p);
		$nList = explode(',', $nList);
		foreach ($nList as $aNode) {
			if ($aNode === '*') {
				foreach ($nodes as $aSecNode => $aux) {
					$nodeIP                   = $nodes[$aSecNode]['ip'];
					$tagList[count($tagList)] = $nodeIP . substr($tag, $p);
				}
			} else {
				$nodeIP                   = $nodes[$aNode]['ip'];
				$tagList[count($tagList)] = $nodeIP . substr($tag, $p);
			}
		}
	} else {
		$tagList = [$tag];
	}
}

function ping($ip) {
	$fp = @fsockopen($ip, 80, $errno, $errstr, 1);
	if ($fp) {
		$status = 1;
		fclose($fp);
	} else {
		$status = 0;
	}
	/* returns 1 if ok */
	return $status;
}

function retrieveJSON($url) {
	set_time_limit(0);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$ret = curl_exec($ch);

	return $ret;
}

function doOnCloud($cmd) {
	parseArguments($src, $dest, $options);
	expandArguments($src, $srcList);
	expandArguments($dest, $destList);

	$myOps = '';
	foreach ($options as $op) {
		$myOps .= "$op ";
	}

	foreach ($srcList as $aSrc) {
		foreach ($destList as $aDest) {
			$theCmd = "$cmd $myOps $aSrc $aDest";
			syslog(LOG_INFO, $theCmd);
			echo shell_exec("$theCmd");
		}
	}
}

if (!isset($cmRequired)) {
	$cmRequired = false;
}
$cfgFile = '/etc/cloudManager.ini';
if (file_exists($cfgFile)) {
	$cfg      = parse_ini_file($cfgFile, true);
	$nodes    = [];
	$nodesAux = $cfg['nodes'];
	foreach ($nodesAux as $nodeName => $nodePort) {
		$nodeInfo         = explode(':', $nodePort);
		$nodes[$nodeName] = ['ip' => $nodeInfo[0], 'port' => $nodeInfo[1]];
	}
	unset($nodePort, $nodeInfo, $nodesAux, $cfg);
} else {
	if ($cmRequired) {
		showError(LOG_ERR, "cloudManager not configured ($cmRequired)\n");
	}
}
