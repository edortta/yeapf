<?php
/*
    skel/webApp/templates/materialize/tasks.php
    YeAPF 0.8.64-11 built on 2021-01-22 16:32 (-3 DST)
    Copyright (C) 2004-2021 Esteban Daniel Dortta - dortta@yahoo.com - MIT License
    2021-01-22 16:32:51 (-3 DST)
*/

  require_once "yeapf.php";

  /* you need to create a CROND entry calling this script as in the next example:
      http://example.com/tasks.php?s=yeapf&a=tick
  */
  if ($s=='yeapf') {
    switch($a) {
      case 'tick':
        require_once "yeapf_ticker.php";
        break;
    }
  }

?>